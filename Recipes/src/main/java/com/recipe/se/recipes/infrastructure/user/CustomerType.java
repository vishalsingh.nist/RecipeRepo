package com.recipe.se.recipes.infrastructure.user;

public enum CustomerType {
    CUSTOMER,
    SELLER;
}
